class Solution():
    def lengthOfLastWord(self, s):
        '''
        count = 0
        for x in range(len(s) - 1, -1, -1):
            if s[x] == " " and count != 0:
                return count
            elif s[x] != " ":
                count += 1
        return count
        '''
        end_index = 0
        top_index = 0
        for x in range(len(s) - 1, -1, -1):
            if s[x] != " " and end_index == 0:
                end_index = x
                top_index = x
            if s[x] == " " and end_index != 0:
                return end_index - x
            if x == 0:
                if s[top_index] != " ":
                    return end_index - x + 1
                else:
                    return end_index - x