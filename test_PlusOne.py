import unittest
from plus_one.PlusOne import Solution


class PlusOneTestCase(unittest.TestCase):
    def test_123(self):
        digits = [1, 2, 3]
        actual = Solution().plusOne(digits)
        excepted = [1, 2, 4]
        self.assertEqual(actual, excepted)

    def test_4321(self):
        digits = [4, 3, 2, 1]
        actual = Solution().plusOne(digits)
        excepted = [4, 3, 2, 2]
        self.assertEqual(actual, excepted)

    def test_0(self):
        digits = [0]
        actual = Solution().plusOne(digits)
        excepted = [1]
        self.assertEqual(actual, excepted)

    def test_沒有資料(self):
        digits = []
        actual = Solution().plusOne(digits)
        excepted = [1]
        self.assertEqual(actual, excepted)


if __name__ == '__main__':
    unittest.main()
