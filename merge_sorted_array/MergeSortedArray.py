class Solution(object):
    def merge(self, nums1, m, nums2, n):
        for x in range(m, len(nums1)):
            nums1[x] = nums2[x - m]

        for x in range(len(nums1)):
            for y in range(x + 1, len(nums1)):
                if (nums1[y] < nums1[x]):
                    k = nums1[y]
                    nums1[y] = nums1[x]
                    nums1[x] = k
