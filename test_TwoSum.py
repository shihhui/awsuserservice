import unittest
from two_sum.TwoSum import Solution


class TwoSumTestCase(unittest.TestCase):
    def test_2_7_eq9(self):
        nums = [2, 7, 11, 15]
        target = 9
        actual = Solution.two_sum(self, nums, target)
        excepted = [0, 1]
        self.assertEqual(actual, excepted)

    def test_2_4_eq6(self):
        nums = [3, 2, 4]
        target = 6
        actual = Solution.two_sum(self, nums, target)
        excepted = [1, 2]
        self.assertEqual(actual, excepted)

    def test_3_3_eq6(self):
        nums = [3, 3]
        target = 6
        actual = Solution.two_sum(self, nums, target)
        excepted = [0, 1]
        self.assertEqual(actual, excepted)


if __name__ == '__main__':
    unittest.main()
