class Solution():
    def plusOne(self, digits):
        k = 1
        j = 0
        result = []

        for x in range(len(digits) - 1, -1, -1):
            j += digits[x] * k
            k = k * 10

        j += 1

        while j != 0:
            result.insert(0, j % 10)
            j = j // 10

        return result