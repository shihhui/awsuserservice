import unittest
from merge_sorted_array.MergeSortedArray import Solution


class MergeSortedArrayTestCase(unittest.TestCase):
    def test_merge_num(self):
        nums1 = [1, 2, 3, 0, 0, 0]
        m = 3
        nums2 = [2, 5, 6]
        n = 3
        Solution().merge(nums1, m, nums2, n)
        actual = nums1
        excepted = [1, 2, 2, 3, 5, 6]
        self.assertEqual(actual, excepted)

    def test_merge_10(self):
        nums1 = [1]
        m = 1
        nums2 = []
        n = 0
        Solution().merge(nums1, m, nums2, n)
        actual = nums1
        excepted = [1]
        self.assertEqual(actual, excepted)

    def test_merge_01(self):
        nums1 = [0]
        m = 0
        nums2 = [1]
        n = 1
        Solution().merge(nums1, m, nums2, n)
        actual = nums1
        excepted = [1]
        self.assertEqual(actual, excepted)


if __name__ == '__main__':
    unittest.main()
