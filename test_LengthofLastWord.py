import unittest
from length_of_last_word.LengthofLastWord import Solution


class LengthofLastWordTestCase(unittest.TestCase):
    def test_HelloWord(self):
        s = "Hello World"
        actual = Solution().lengthOfLastWord(s)
        excepted = 5
        self.assertEqual(actual, excepted)

    def test_a(self):
        s = "a "
        actual = Solution().lengthOfLastWord(s)
        excepted = 1
        self.assertEqual(actual, excepted)

    def test_ba(self):
        s = "b   a    "
        actual = Solution().lengthOfLastWord(s)
        excepted = 1
        self.assertEqual(actual, excepted)

    def test_space(self):
        s = " "
        actual = Solution().lengthOfLastWord(s)
        excepted = 0
        self.assertEqual(actual, excepted)

    def test_space(self):
        s = "day"
        actual = Solution().lengthOfLastWord(s)
        excepted = 3
        self.assertEqual(actual, excepted)


if __name__ == '__main__':
    unittest.main()
