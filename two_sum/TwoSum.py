class Solution():
    def two_sum(self, nums, target):
        '''
        for x in range(len(nums)):
            for y in range(x + 1, len(nums)):
                if nums[x] + nums[y] == target:
                    return [x, y]
        '''
        dic = {}
        for x in range(len(nums)):
            if target - nums[x] in dic:
                return [dic[target - nums[x]], x]
            dic[nums[x]] = x
